/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var QueueController = Stapes.create().extend({
	
	"queueProcessStatus" : 'STARTED',
	
	"load" : function() {
		
        var storage = window.localStorage;
        var queue = $.parseJSON(storage.getItem('queue'));
        
        if (queue==null)
        	queue = [];		
		
        //Log
        console.log("Queue Controller: init: value of queue " + JSON.stringify(queue));  
        
        $('#queue').html('');
        
        var row = '';
        row += '<input  data-theme="a" type="checkbox" name="checkboxa" id="checkboxa" class="custom"/>';
        row += '<label for="checkboxa">';
        row += '<div class="grid_4">Date</div>';
        row += '<div class="grid_4">Trailer</div>';
        row += '<div class="grid_4">Status</div>';
        row += '</label>';
        $('#queue').append(row);
        
        for (var index in queue) {
        	if (queue[index] != null) {
        		var trailerid = '';
        		var trailertype = '';
        		var theme = '';
        		if (queue[index].type == 'SURVEY') {
        			theme = 'd';
		        	var reTrailerId = /trailerid=([^&]{1,})/g;
		        	var reTrailerType = /trailertype=([^&]{1,})/g;
		        	trailerid = reTrailerId.exec(queue[index].value);
		        	if (trailerid != null)
		        		if (trailerid.length >= 2)
		        	        trailerid = trailerid[1];
		        	
		        	trailertype = reTrailerType.exec(queue[index].value)[1];
		        	if (trailertype != null)
		        		if (trailertype.length >= 2)
		        	        trailertype = trailertype[1];
        		} else {
        			theme = 'e';
        			trailerid = queue[index].value.params.trailerid;
        			trailertype = queue[index].value.params.trailertype;
        		}
	            var created = new Date(queue[index].created);
	            row = '';
				row += '<input data-theme="' + theme + '" type="checkbox" name="checkbox-' + queue[index].id + '" id="checkbox-' + queue[index].id + '" class="custom"/>';
				row += '<label for="checkbox-' + queue[index].id + '">';
				row += '<div class="grid_4">' + created.getDate() + '-' + (created.getMonth()+1) + '</div>';
				row += '<div class="grid_4">' + trailerid + '</div>';
				row += '<div class="grid_4">' + queue[index].status + '</div>';
				row += '</label>';  
			    console.log( 'Queue Controller: init: showing queue list ' + row); 
			    $('#queue').append(row);
        	}
		    
        }
        
        if (queue.length==0) {
            var row = '';
			row += '<input disabled="disabled" data-theme="d" type="checkbox" name="checkbox-d" id="checkbox-d" class="custom"/>';            
            row += '<label for="checkbox-d">';
			row += '<div class="grid_12" style="text-align:center;">--No Pending Survey(s) to report--</div>';
			row += '</label>'; 
		    console.log( 'Queue Controller: init: showing queue list ' + row); 
		    $('#queue').append(row);        	
        }
        
        $('#queue').trigger('create');
        
        //Log
	    console.log( 'Queue Controller: init: complete queue ' + $('#queue').html());		
	},
	
    "init" : function() {
        
        //Log
        console.log("Queue Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        UserModel.init();
        HelpDeskModel.init();
        
        this.load();
        	    
	    $('#checkboxa').click(function(){
	    	
	        //Log
		    console.log( 'Queue Controller: init: $(#checkboxa).click ' +  $('input[name^=checkbox-]').length);	    	

	    	$('input[name^=checkbox-]').attr('checked', this.checked).checkboxradio("refresh");
	    	
	    });
	    
	    var that = this;
	    
	    $('#button-start_upload').click(function(){
	    	
	        //Log
		    console.log( 'Queue Controller: init: $(#button-start_upload).click ');	    	

		    that.queueProcessStatus = 'STARTED';
		    
		    that.emit('processQueue');
	    	
	    });
	    
	    $('#button-pause_upload').click(function(){
	    	
	        //Log
		    console.log( 'Queue Controller: init: $(#button-pause_upload).click ');	    	

		    that.queueProcessStatus = 'PAUSED';
	    	
	    });	    
	    
	    $('#button-delete_queue').click(function(){
	    	 
	        //Log
		    console.log( 'Queue Controller: init: $(#button-delete_queue).click ');	    	

	        var ids = [];    
	        $('input[name^=checkbox-]').filter(':checked').each(function () {	
	        	console.log('Queue Controller: init: #button-delete_queue attr-id Replaced: ' + this.id.toString().replace('checkbox-',''));
	        	ids.push(this.id.toString().replace('checkbox-',''));
	        });
	        
	        HelpDeskModel.deleteFromQueue(ids);
	        
	        that.load();
	        
	    });	 	    
	    
        //Log
        console.log("Queue Controller: init: End");        
    },

});

QueueController.on('processQueue', function(){
	
    //Log
    console.log("Queue Controller: process Queue: Start status:" + this.queueProcessStatus); 	
	
	if (this.queueProcessStatus=='STARTED') {
		var storage = window.localStorage;
		var queue = $.parseJSON(storage.getItem('queue'));
		var that = this;	
		
	    //Log
	    console.log("Queue Controller: process Queue: Queued Items Count:" + $('input[name^=checkbox-]').length);
		
		for (var index in queue) {
            //Log
            console.log("Queue Controller: process Queue: Loop Start loopid-" + queue[index].id + '--');			
			
	    	if (queue[index].type == 'SURVEY' && queue[index].status == 'PENDING') {
	    		
	            //Log
	            console.log("Queue Controller: process Queue: type SURVEY: Found id " + queue[index].id); 		    		
	    		
	    		var successCallback = function(response) {
	    			
	    	        //Log
	    	        console.log("Queue Controller: process Queue: type SURVEY: Success Found id " + queue[index].id); 		    			
	    			
	    			queue[index].status = 'UPLOADED';
	    			storage.setItem('queue', JSON.stringify(queue)); 
	    			
	    			$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html(queue[index].status);
	    			
	    			that.emit('processQueue');
	    		};
	    		
	    		var errorCallback = function(jqXHR, textStatus, errorThrown) {
	    			
	    	        //Log
	    	        console.log("Queue Controller: process Queue: type SURVEY: Error Found id " + queue[index].id + ' ' + jqXHR.responseText);		    			
	    			
	    			queue[index].status = 'ERROR';
	    			storage.setItem('queue', JSON.stringify(queue)); 
	    			
	    			$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html(queue[index].status);
	    			
	    			that.emit('processQueue');
	    		};
	    		
    			queue[index].status = 'PROCESSING ...';
    			storage.setItem('queue', JSON.stringify(queue)); 	    		
	    		
	    		$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html('PROCESSING ...');
	    		
	    	    HelpDeskModel.reportSurvey(queue[index].value, successCallback, errorCallback);
	    	    return false;
	    	}//if type survey
	    	
	    	if (queue[index].type == 'DAMAGE' && queue[index].status == 'PENDING') {
	    		
	            //Log
	            console.log("Queue Controller: process Queue:  type DAMAGE: Found id " + queue[index].id); 		    		
	    		
	    		var successCallback = function(response) {	    			
	    			that.emit('processQueue');
	    			
	    	        //Log
	    	        console.log("Queue Controller: successCallback: type DAMAGE: start ");	    			
	    			
	    	        if (response.responseCode == '200'){
		    	        //Log
		    	        console.log("Queue Controller: successCallback:  type DAMAGE: Success Found id " + queue[index].id);	    	        	
	    	        	
	    	        	queue[index].status = 'UPLOADED';
	    				storage.setItem('queue', JSON.stringify(queue));
	    	        } else {
		    	        //Log
		    	        console.log("Queue Controller: successCallback:  type DAMAGE: Error Found id " + queue[index].id + ' ' + response.response);		    			
		    			
		    			queue[index].status = 'ERROR';
		    			storage.setItem('queue', JSON.stringify(queue)); 	    	        	
	    	        }
	    			
	    			$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html(queue[index].status);
	    			
	    			that.emit('processQueue');
	    			
	    	        //Log
	    			
	    	        console.log("Queue Controller: successCallback: type DAMAGE: end ");	    			
	    			
	    		};
	    		
	    		var errorCallback = function(error) {
	    			
	    	        //Log
	    	        console.log("Queue Controller: errorCallback: type DAMAGE: end " + error.code + ' ' + error.source + ' ' + error.target + ' ' + error.http_status);	
	    	        
	    			queue[index].status = 'ERROR';
	    			storage.setItem('queue', JSON.stringify(queue)); 	    	        
	    			
	    			$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html(queue[index].status);
	    			
	    			that.emit('processQueue');
	    		};
	    		
    			queue[index].status = 'PROCESSING ...';
    			storage.setItem('queue', JSON.stringify(queue)); 	    		
	    		
	    		$('label[for=checkbox-' + queue[index].id + ']').find('div:nth-child(3)').html('PROCESSING ...');
	    		
	    	    HelpDeskModel.reportDamage(queue[index].value, successCallback, errorCallback);
	    	    return false;
	    	}//if type damage	    	
	    	
		} //for
        //Log
        console.log("Queue Controller: process Queue: End"); 		
	}
});
