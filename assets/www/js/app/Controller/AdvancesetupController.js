/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var AdvancesetupController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("Advancesetup Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        
        //Rendering the view
        AccountView.render();
        
        //Binding event change to form elements
        $('#button-save').unbind('click').bind('click', function(){
            
            //Log
            console.log("Advancesetup Controller: $('#button-save').bind('click'): Start");
            
             AccountModel.apikey = $('#apikey').val();
             AccountModel.apisecret = $('#apisecret').val();
             AccountModel.url = $('#url').val();
             AccountModel.scheme = $('input[name=scheme]:checked').val();
             AccountModel.imagequality = $('#imagequality').val();             
             AccountModel.save();
             
             //Log
             console.log("Advancesetup Controller: $('#button-save').bind('click'): Start");
        });
        
        //Binding event change to form elements
        $('#button-updatecache').unbind('click').bind('click',function(){
        	
        });
        
        //Log
        console.log("Advancesetup Controller: init: End");        
    }
});