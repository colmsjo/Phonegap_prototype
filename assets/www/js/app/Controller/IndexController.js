/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var IndexController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("Index Controller: init: Start");

        var switchPage = function () {
        	if (UserModel.isSessionActive()) 
                $.mobile.changePage("survey.html", {transition: 'none'});
        	else 
        		$.mobile.changePage("setup.html", {transition: 'none'});
        };

        setTimeout(switchPage, 5000);
        
        //Log
        console.log("Index Controller: init: End");        
     
    }
});