/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var DamagedetailsController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("Damagedetails Controller: init: Start");
        
        console.log("Damagedetails Controller: init: Damage Details" + window.localStorage.getItem('damage_details'));
        
        var damage_details = $.parseJSON(window.localStorage.getItem('damage_details'));
        
        for (var key in damage_details.result) {
        	
        	//Log
            console.log("Damagedetails Controller: itiration:  " + key + ' VALUE ' + damage_details.result[key] + ' count ' + $("#page-damagedetails #" + key).length);         	
        	
        	$("#page-damagedetails #" + key).html(damage_details.result[key]);
        	
        }
        
        for (var index in damage_details.result.documents) {
            //Log
            console.log("Damagedetails Controller: document itiration:  start " + index);            

            //Sending request to fetch picklist
            RequestBehaviour.send(
                'GET',
                'DocumentAttachments',
                damage_details.result.documents[index].id,
                function(data){
                	$('#image-list').append('<img style="width:50px;height:50px;" src="data:image/png;base64,' + data.result.filecontent + '"></img>');
                },
                function(){}
            );
            
            //Log
            console.log("Damagedetails Controller: document itiration:  End");         	
        }
        
        //Log
        console.log("Damagedetails Controller: init: End ");        
    }
});