/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var SurveyController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("Survey Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        UserModel.init();
        HelpDeskModel.init();
        AssetModel.init();
        
        $('#trailertype').unbind('change').change(function(){
        	console.log('Survey Controller: init: trailertype change event start');
            //Trailer Type
            var assetlist = $.parseJSON(AssetModel.getList());
            
        	$('#trailerid').html('');            
            for (var index in assetlist) {
            	//console.log('Survey Controller: init: trailertype change event ' +  assetlist[index].trailertype+' '+$('#trailertype option:selected').text());
            	if (assetlist[index].trailertype==$('#trailertype option:selected').text())
                    $('#trailerid').append('<option value="' + assetlist[index].assetname + '">' + assetlist[index].assetname + '</option>');
            }
            $('#trailerid').selectmenu('refresh');
            console.log('Survey Controller: init: trailertype change event end');
        });
        
        
        //Trailer Type
        var trailertypes = $.parseJSON(AssetModel.getPicklist('trailertype'));
        
        for (var index in trailertypes) {
            $('#trailertype').append('<option value="' + trailertypes[index].label + '">' + trailertypes[index].value + '</option>');
        }
        $('#trailertype').selectmenu('refresh');
        
        //AssetList
        var assetlist = $.parseJSON(AssetModel.getList());
        
    	$('#trailerid').html('');            
        for (var index in assetlist) {
        	//console.log('Survey Controller: init: trailertype change event ' +  assetlist[index].trailertype+' '+$('#trailertype option:selected').text());
        	if (assetlist[index].trailertype==$('#trailertype option:selected').text())
                $('#trailerid').append('<option value="' + assetlist[index].assetname + '">' + assetlist[index].assetname + '</option>');
        }     
        $('#trailerid').selectmenu('refresh');        
        
        //Place
        var damagereportlocation = $.parseJSON(HelpDeskModel.getPicklist('damagereportlocation'));
        
        for (index in damagereportlocation) {
            $('#damagereportlocation').append('<option value="' + damagereportlocation[index].label + '">' + damagereportlocation[index].value + '</option>');
        }
        $('#damagereportlocation').selectmenu('refresh'); 
        
        //Straps
        var straps = $.parseJSON(HelpDeskModel.getPicklist('straps'));
        
        for (index in straps) {
            $('#straps').append('<option value="' + straps[index].label + '">' + straps[index].value + '</option>');
        }
        $('#straps').selectmenu('refresh'); 
        
        //Plates
        var plates = $.parseJSON(HelpDeskModel.getPicklist('plates'));
        
        for (index in plates) {
            $('#plates').append('<option value="' + plates[index].label + '">' + plates[index].value + '</option>');
        } 
        $('#plates').selectmenu('refresh'); 
        
        //Sealed
        var sealed = $.parseJSON(HelpDeskModel.getPicklist('sealed'));
        $('#sealed').html('<legend>Sealed</legend>');
        var selected = '';
        for (index in sealed) {
        	if (index==0) 
        		selected = 'checked="checked"';
        	else
        		selected = '';
        	
        	console.log('Survey Controller: init: populating sealed ' + sealed[index].value);
        	
            $('#sealed').append('<input type="radio" name="sealed" value="' + sealed[index].value + '" id="sealed-' + index + '" ' + selected + ' />');
            $('#sealed').append('<label for="sealed-' + index + '">' + sealed[index].label + '</label>');
        }
        $('#sealed').trigger('create');
        $('#sealed').controlgroup();
        
        $('#trailertype').unbind('change').change(function(){
        	console.log('Survey Controller: init: trailertype change event start');
            //Trailer Type
            var assetlist = $.parseJSON(AssetModel.getList());
            
        	$('#trailerid').html('');            
            for (var index in assetlist) {
            	//console.log('Survey Controller: init: trailertype change event ' +  assetlist[index].trailertype+' '+$('#trailertype option:selected').text());
            	if (assetlist[index].trailertype==$('#trailertype option:selected').text())
                    $('#trailerid').append('<option value="' + assetlist[index].assetname + '">' + assetlist[index].assetname + '</option>');
            }
            $('#trailerid').selectmenu('refresh');
            console.log('Survey Controller: init: trailertype change event end');
        });        
        
        $('#sealed').unbind('change').change(function(){
        	console.log('Survey Controller: init: #sealed change event start value ' + $('#sealed input:checked').val());
        	if ($('#sealed input:checked').val()=='Ja' || $('#sealed input:checked').val()=='Yes') {
	        	$('#plates').parents('.grid_12:first').hide();
	        	$('#straps').parents('.grid_12:first').hide();
	        	$('h3:contains(Trailer Inventory)').hide();
	        	
        		$('#plates').selectmenu('disable');
        		$('#straps').selectmenu('disable'); 	        	
        	} else {
        		$('#plates').selectmenu('enable');
        		$('#straps').selectmenu('enable');
        		
	        	$('#plates').parents('.grid_12:first').show();
	        	$('#straps').parents('.grid_12:first').show();
	        	$('h3:contains(Trailer Inventory)').show();        		
        	}
            console.log('Survey Controller: init: #sealed change event end');
        });        
        
        //Form Submit
        $('#button-nodamage').unbind('click').bind('click', function(){
        	
        	//Log
        	console.log('Survey Controller: init: #button-nodamage click: adding to queue ' + $('#survey').serialize());
        	
        	HelpDeskModel.addToQueue($('#survey').serialize(), 'SURVEY');
        	
        	QueueController.emit('processQueue');
        	
        	alert('Survey added to queue successfully');
        });
        
        //Form Submit
        $('#button-damage').unbind('click').bind('click', function(){
        	
        	//Log
        	console.log('Survey Controller: init: #button-damage click: saving ' + $('#survey').serialize());
        	
        	var storage = window.localStorage;        	
        	storage.setItem('current_damage', JSON.stringify($('#survey').serializeObject()));
        	
            //redirect to different page
        	$.mobile.changePage("damages.html", { transition: "none"});
        	
        });        
        
    	$('#plates').parents('.grid_12:first').hide();
    	$('#straps').parents('.grid_12:first').hide();
    	$('h3:contains(Trailer Inventory)').hide();
    	
		$('#plates').selectmenu('disable');
		$('#straps').selectmenu('disable');         
        
        //Log
        console.log("Survey Controller: init: End");        
    }
});
