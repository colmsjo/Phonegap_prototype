/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var UserController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("User Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        UserModel.init();
        HelpDeskModel.init();
        AssetModel.init();
        
        //Binding event click for login button
        $('a:contains(Login)').click(function(){
            var errorCallback,
                successCallback;
                
            //Show Popup
            $('#message').html('<h3 style="color:#FFA500;">Logging In ...</h3>');                  
                
            //Clear the fetch count
            HelpDeskModel.picklistCachedCount = 0;
            HelpDeskModel.picklistNoCachedCount = 0;
            
            AssetModel.picklistCachedCount = 0;
            AssetModel.picklistNoCachedCount = 0;
                
            //Show Popup
            $('#popupBasic').popup('open');
                
            //Log
            console.log("Click: a:contains(Login): Start");
            
            //If error in loggin in run the following
            errorCallback = function(jqXHR, textStatus, errorThrown) {
            	var response;
            	var message = 'Unable to Login';
            	
            	if (jqXHR.responseText!='') {
            		response = $.parseJSON(jqXHR.responseText);
            		message = response.error.message;
            	}
            	
            	if (jqXHR.status == 0) {
            		message = "Unable to connect to server"
            	}
                
                //Show Popup
                $('#message').html('<h3 style="color:red;">' + message + '</h3>');
                
                //Log
                console.log(
                    'UserController: errorCallback:' + jqXHR.status + ' ' + textStatus
                    + ' ' + jqXHR.responseText + ' ' + errorThrown
                ); 
            };
            
            //Successful login
            successCallback = function(response) {
                
                var storage = window.localStorage,
                    index2;                
                
                //Log
                console.log('UserController: successCallback:' + JSON.stringify(response));                         
                
                //If response is good store it else
                //clear the keys
                if (response.success) {
                    
                    var successHelpDeskPicklistCallback,
                        errorHelpDeskPicklistCallback,
                        successAssetPicklistCallback,
                        errorAssetPicklistCallback;                        
                    
                    //Show Popup
                    $('#message').html('<h3 style="color:green;">Login Successfull! Building cache .. Please wait</h3>');
                    
                    //save the credentails for the session
                    UserModel.saveCredentials();
                    
                    //fetch and cache all picklists
                    for(var index1 = 0 in HelpDeskModel.picklists){
                        
                        var picklistvalue = HelpDeskModel.getPicklist(HelpDeskModel.picklists[index1]);
                        
                        //Log
                        console.log('UserController: successCallback: Fetch Picklist Loop: ' + index1 + ' ' + HelpDeskModel.picklists[index1] + '  ' +  picklistvalue);                        
                        
                        if (picklistvalue==null) {                                                   
                            var context = function(indexi){
                                
                                //callback for success of fetch picklist request
                                successHelpDeskPicklistCallback = function(response) {

                                    //Log
                                    console.log("UserController: successCallback: successHelpDeskPicklistCallback: start");

                                    if (response.success==true) {

                                        //Log
                                        console.log("UserController: successCallback: successHelpDeskPicklistCallback: saving " + indexi + ' ' + HelpDeskModel.picklists[indexi] + '  ' + JSON.stringify(response.result));

                                        HelpDeskModel.setPicklist(
                                            HelpDeskModel.picklists[indexi], 
                                            response.result
                                        );

                                    
                                    }

                                    //Log
                                    console.log("UserController: successCallback: successHelpDeskPicklistCallback: end");
                                    
                                    UserModel.emit('cachereceived', ['HelpDesk', 'success']);
                                };

                                errorHelpDeskPicklistCallback = function(jqXHR, textStatus, errorThrown) {

                                    //Log
                                    console.log(
                                        'UserController: successCallback: errorHelpDeskPicklistCallback' + 
                                        jqXHR.status + ' ' + 
                                        textStatus + ' ' + 
                                        jqXHR.responseText + ' ' + 
                                        errorThrown
                                    ); 

                                    UserModel.emit('cachereceived', ['HelpDesk', 'error']);

                                };                                 
                                
                                
                                HelpDeskModel.fetchPicklist(
                                    HelpDeskModel.picklists[indexi],
                                    successHelpDeskPicklistCallback,
                                    errorHelpDeskPicklistCallback
                                );
                            };
                            context(index1);
                            
                        } else {
                            UserModel.emit('cachereceived', ['HelpDesk', 'success']);
                        }
                    }
                    
                    index1=0;
                    
                    for(index1 in AssetModel.picklists) {
                        
                        picklistvalue = AssetModel.getPicklist(AssetModel.picklists[index1]);
                        
                        //Log
                        console.log('UserController: successCallback: Fetch Picklist Loop: ' + index1 + ' ' + AssetModel.picklists[index1] + '  ' +  picklistvalue);                        
                        
                        if (picklistvalue==null) {                                                   
                            context = function(indexi){
                                
                                //callback for success of fetch picklist request
                                successAssetPicklistCallback = function(response) {

                                    //Log
                                    console.log("UserController: successCallback: successAssetPicklistCallback: start");

                                    if (response.success==true) {

                                        //Log
                                        console.log("UserController: successCallback: successAssetPicklistCallback: saving " + indexi + ' ' + AssetModel.picklists[indexi] + '  ' + JSON.stringify(response.result));

                                        AssetModel.setPicklist(
                                            AssetModel.picklists[indexi], 
                                            response.result
                                        );

                                        UserModel.emit('cachereceived', ['Asset', 'success']);                                   
                                    }

                                    //Log
                                    console.log("UserController: successCallback: successAssetPicklistCallback: end");
                                };

                                errorAssetPicklistCallback = function(jqXHR, textStatus, errorThrown) {

                                    //Log
                                    console.log(
                                        'UserController: successCallback: errorAssetPicklistCallback' + 
                                        jqXHR.status + ' ' + 
                                        textStatus + ' ' + 
                                        jqXHR.responseText + ' ' + 
                                        errorThrown
                                    ); 

                                    UserModel.emit('cachereceived', ['Asset', 'error']);

                                };                                 
                                
                                
                                AssetModel.fetchPicklist(
                                    AssetModel.picklists[indexi],
                                    successAssetPicklistCallback,
                                    errorAssetPicklistCallback
                                );
                            };
                            context(index1);
                            
                        } else {
                            UserModel.emit('cachereceived', ['Asset', 'success']);  
                        }
                    } 
                    
                    
                    /**
                     * Get List of values
                     */
                    var successAssetListCallback,
                        errorAssetListCallback;
                    var assetlist = AssetModel.getList();

                    //Log
                    console.log('UserController: successCallback: Fetch AssetList' + assetlist);                        

                    if (assetlist==null) {                                                   

                            //callback for success of fetch picklist request
                            successAssetListCallback = function(response) {

                                //Log
                                console.log("UserController: successCallback: successAssetListCallback: start");

                                if (response.success==true) {

                                    //Log
                                    console.log('UserController: successCallback: successAssetListCallback: saving ' + JSON.stringify(response.result));

                                    AssetModel.setList(
                                        response.result
                                    );

                                    UserModel.emit('cachereceived', ['AssetList', 'success']);                                   
                                }

                                //Log
                                console.log("UserController: successCallback: successAssetListCallback: end");
                            };

                            errorAssetListCallback = function(jqXHR, textStatus, errorThrown) {

                                //Log
                                console.log(
                                    'UserController: successCallback: errorAssetListCallback' + 
                                    jqXHR.status + ' ' + 
                                    textStatus + ' ' + 
                                    jqXHR.responseText + ' ' + 
                                    errorThrown
                                ); 

                                UserModel.emit('cachereceived', ['AssetList', 'error']);

                            };                                 


                            AssetModel.fetchList(
                                successAssetListCallback,
                                errorAssetListCallback
                            );

                    } else {
                        UserModel.emit('cachereceived', ['AssetList', 'success']);  
                    }                    
              
                } else {
                    
                    //Show Popup
                    $('#message').html('<h3 style="color:green;">Login Successfull! Building cache .. Please wait</h3>');                    
                    
                    UserModel.clearCredentials();
                }
                
            };            
            

            UserModel.login(
                $('#username').val(), 
                $('#password').val(), 
                errorCallback, 
                successCallback
            );
             
            //Log
            console.log("Click: a:contains(Login): End");
        });
        
        //Log
        console.log("User Controller: init: End");        
    }
});
UserController.init();