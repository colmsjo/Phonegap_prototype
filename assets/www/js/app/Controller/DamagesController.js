/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var DamagesController = Stapes.create().extend({
    "init" : function() {
        var that = this;
        //Log
        console.log("Damages Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        UserModel.init();
        HelpDeskModel.init();

    	var current_damage_details = $.parseJSON(window.localStorage.getItem('current_damage')); 
    	
        //Log
        console.log("Damages Controller: init: current damage details: " + current_damage_details.trailerid);
        //window.localStorage.removeItem('damages_' + current_damage_details.trailerid);
    	var troubletickets = window.localStorage.getItem('damages_' + current_damage_details.trailerid);
    	
        //Log
        console.log("Damages Controller: init: get damage details: " + troubletickets);    	    	
    	
        $('#button-refresh').unbind('click').bind('click', function(){
        	
        	var that_current_damage_details = current_damage_details;
            //Log
            console.log("Damages Controller: init: $('#button-refresh').bind('click') ");        	
        	
            $('#troubletickets').find('li:first').siblings().remove();
	        $('#troubletickets').append('<li><center>Loading...</center></li>');
			$('#troubletickets').listview('refresh');
			
            var sucessCallback = function(response){
            	
                //Log
                console.log("Damages Controller: init: sucessCallback: tts received " + response.result.length);        	
            	
                window.localStorage.setItem('damages_' + that_current_damage_details.trailerid, JSON.stringify(response.result));
                
            	that.render(response.result);          	
            };
            
    		var errorCallback = function(jqXHR, textStatus, errorThrown) {
    			
    	        //Log
    	        console.log("Damages Controller: init: errorCallback: " +  jqXHR.responseText);		
    	        
    	        $('#troubletickets').find('li:first').siblings().remove();
    	        $('#troubletickets').append('<li><center>--No damages reported--</center></li>');
    			$('#troubletickets').listview('refresh');
    			
    		};   
    		
            //Log
            console.log("Damages Controller: init: fetching: " + 'damaged/0000/00/' + current_damage_details.trailerid + '/all');    		
	        
	        HelpDeskModel.fetchList('damaged/0000/00/' + current_damage_details.trailerid + '/all', sucessCallback, errorCallback);
        });
        
        $('#button-report').unbind('click').bind('click', function(){
        	
            //redirect to different page
            $.mobile.changePage("damageadd.html", {transition: 'none'});        	
        });
       
    	if (troubletickets != null) {
    		
            //Log
            console.log("Damages Controller: init: Trouble Tickets found " + troubletickets);    		
    		
    		this.render($.parseJSON(troubletickets));
    	} else {
    		
            //Log
            console.log("Damages Controller: init: Trouble Tickets not found : triggering click");    		
    		
    		$('#button-refresh').trigger('click');
    	}        
        
        //Log
        console.log("Damages Controller: init: End " + $('#button-report').length);        
    },
    
    'render' : function(troubletickets){
		var item = '';            
        
		$('#troubletickets').find('li:first').siblings().remove();
    	for (var index in troubletickets) {
    		item += '<li><a id="' + troubletickets[index].id + '" href="javascript:void(0);">'+ troubletickets[index].damageposition + ' : ' + troubletickets[index].damagetype + ' : ' + troubletickets[index].ticket_title + '</a></li>';
    	}
		$('#troubletickets').append(item); 
		$('#troubletickets').listview('refresh');
    	
    	$('li a').click(function(e){
    		e.preventDefault();       		
    		
    		var successDetailsCallback = function(data){
    			
                //Log
                console.log("Damages Controller: init: successDetailsCallback: start " +  JSON.stringify(data));        			
    			
    			window.localStorage.setItem('damage_details', JSON.stringify(data));
    			
                //redirect to different page
                $.mobile.changePage("damagedetails.html", {transition: 'none'});  
                
                //Log
                console.log("Damages Controller: init: successDetailsCallback: end ");                    
    			
    		};
    		
    		var errorDetailsCallback = function(){
    			
    			alert('Error fetching details');
    			
    		};        		
    		
    	    HelpDeskModel.fetchId(this.id, successDetailsCallback, errorDetailsCallback);         		
    		
    		return false;
    	});     	
    }
});