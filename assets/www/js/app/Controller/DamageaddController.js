/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var DamageaddController = Stapes.create().extend({
    "init" : function() {
    	
    	var index;
        
        //Log
        console.log("Damage Add Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        UserModel.init();
        HelpDeskModel.init();
        AssetModel.init();
        
        $('#damagetype').unbind('change').change(function(){
        	console.log('Damage Add Controller: init: damagetype change event start');

            console.log('Damage Add Controller: init: trailertype change event end');
        });
        
        //Damage Type
        var damagetype = $.parseJSON(HelpDeskModel.getPicklist('damagetype'));
        
        for (index in damagetype) {
            $('#damagetype').append('<option value="' + damagetype[index].label + '">' + damagetype[index].value + '</option>');
        }
        $('#damagetype').selectmenu('refresh');
        
        //Damage Type
        var damageposition = $.parseJSON(HelpDeskModel.getPicklist('damageposition'));
        
        for (index in damageposition) {
            $('#damageposition').append('<option value="' + damageposition[index].label + '">' + damageposition[index].value + '</option>');
        }
        $('#damageposition').selectmenu('refresh'); 
        
        //Driver Caused damage
        var drivercauseddamage = $.parseJSON(HelpDeskModel.getPicklist('drivercauseddamage'));
        
        for (index in drivercauseddamage) {
            $('#drivercauseddamage').append('<option value="' + drivercauseddamage[index].label + '">' + drivercauseddamage[index].value + '</option>');
        }
        $('#drivercauseddamage').selectmenu('refresh');         
        
        //Adding New Images
        $('#button-addimage').unbind('click').click(function(){
        	
        	//Log
        	console.log('Damage Add Controller: init: #button-add-image click: start');
        	
        	function onSuccess(imageURL) {
        		
        		//Log
        		console.log("Damage Add Controller: init: OnSuccess: image received: " + imageURL);
        	    
        		$('#damage-images').append('<img class="image-to-upload" style="width:50px;height:50px;" src="' + imageURL + '"/>');
        	}

        	function onFail(message) {
        		
        		//Log
        		console.log("Damage Add Controller: init: OnFail: " + message);        		
        		
        	    alert('Failed because: ' + message);
        	}         	
        	
        	navigator.camera.getPicture(onSuccess, onFail, { quality: 50,
        	    destinationType: Camera.DestinationType.FILE_URL
        	});        	
        	
        	//Log
        	console.log('Damage Add Controller: init: #button-add-image click: end');
        });
        
        //Form Submit
        $('#button-done').unbind('click').click(function(){
        	
        	//Log
        	console.log('Damage Add Controller: init: #button-done click: ' + $('#damage-add').serialize());
        	
        	var storage = window.localStorage;        	
        	var queue_record_value = new Object();
        	var damage_value = $('#damage-add').serializeObject();
        	
        	queue_record_value.params = $.parseJSON(storage.getItem('current_damage'));
        	queue_record_value.params.drivercauseddamage = damage_value.drivercauseddamage;
        	queue_record_value.params.damageposition = damage_value.damageposition;
        	queue_record_value.params.damagetype = damage_value.damagetype;
        	
        	//Log
        	console.log('Damage Add Controller: init: #button-done click: complete params ' + JSON.stringify(queue_record_value.params));        	
        	
        	queue_record_value.files = [];
        	
        	console.log('Damage Add Controller: init: #button-done click: No of Images ' + $('#damage-images img').length);        	
        	$('#damage-images img').each(function(){
        		queue_record_value.files.push(this.src);
        	});
        	
        	HelpDeskModel.addToQueue(queue_record_value, 'DAMAGE');
        	
        	console.log('Damage Add Controller: init: #button-done click: ' + JSON.stringify(queue_record_value.params));
        	        	
        	QueueController.emit('processQueue');        	
        	
            //redirect to different page
        	$.mobile.changePage("queue.html", { transition: "none"});
        	
        });        
        
        //Log
        console.log("Damage Add Controller: init: End");        
    }
});
