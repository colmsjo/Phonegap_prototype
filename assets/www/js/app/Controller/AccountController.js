/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var AccountController = Stapes.create().extend({
    "init" : function() {
        
        //Log
        console.log("Account Controller: init: Start");

        //Initiating Model
        AccountModel.init();
        
        //Rendering the view
        AccountView.render();
        
        //Binding event change to form elements
        $('#apikey, #apisecret, #url').blur(function(){
            
            //Log
            console.log("Blur: #apikey #apisecret #url: Start");
            
             AccountModel.apikey = $('#apikey').val();
             AccountModel.apisecret = $('#apisecret').val();
             AccountModel.url = $('#url').val();
             AccountModel.save();
             
            //Log
            console.log("Blur: #apikey #apisecret #url: End");
        });
        
        //Log
        console.log("Account Controller: init: End");        
    }
});
AccountController.init();