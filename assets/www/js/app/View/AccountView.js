/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var AccountView = Stapes.create().extend({

    "init" : function() {
        
    },
    "render" : function() {
        
        //Log
        console.log("AccountView: Render: Start #apikey #apisecret");        
        
        console.log(
            "AccountView: Render: Value Found #apikey:" + 
            AccountModel.apikey +
            " #apisecret" + AccountModel.apisecret +
            " #url" + AccountModel.url +
            " #scheme" + AccountModel.scheme +
            " #imagequality" + AccountModel.imagequality
        ); 
        
        $('#apikey').val(AccountModel.apikey);
        $('#apisecret').val(AccountModel.apisecret);
        $('#url').val(AccountModel.url);
        $('#scheme').val(AccountModel.scheme);
        $('#imagequality').val(AccountModel.imagequality);
        
        //Log
        console.log("AccountView: Render: End #apikey #apisecret"); 
    }    
});