/**
 * @fileoverview Model file for HelpDesk
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 28th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var AssetModel = Stapes.create().extend({
    "fields" : '',

    "picklists" : [
        'trailertype'
    ],
    
   "picklistCachedCount" : 0, 
   
   "picklistNoCachedCount" : 0,

   "listCached" : null,

    "init" : function () {

    },

    "clearPicklist" : function (fieldname) {
        var storage = window.localStorage;
        
        //Log
        console.log("AssetModel: clearPicklist: clear " + fieldname);

        storage.removeItem(
            'picklist_' + fieldname
        );
    },

    "setPicklist" : function (fieldname, value) {
        var storage = window.localStorage;
        
        //Log
        console.log("AssetModel: setPicklist: saving " + fieldname + " " + JSON.stringify(value));

        storage.setItem(
            'picklist_' + fieldname, 
            JSON.stringify(value)
        );
    },

    "getPicklist" : function (fieldname) {
        var storage = window.localStorage;
        
        //Log
        console.log("AssetModel: getPicklist: " + fieldname + ' value-from-localstorage' + storage.getItem('picklist_' + fieldname));

        return storage.getItem('picklist_' + fieldname);        
    },

    "fetchPicklist" : function (fieldname, successCallback, errorCallback) {
            
        //Log
        console.log("AssetModel: getPicklists: fetching picklist for " + fieldname);            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'GET',
            'Assets',
            fieldname,
            successCallback,
            errorCallback
        );
        
        //Log
        console.log("AssetModel: getPicklist: End");        

    },

    "fetchList" : function (successCallback, errorCallback) {
            
        //Log
        console.log("AssetModel: getLists: fetching all assets ");            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'GET',
            'Assets',
            '',
            successCallback,
            errorCallback
        );
        
        //Log
        console.log("AssetModel: getList: End");        

    },


    "setList" : function (value) {
        var storage = window.localStorage;
        
        //Log
        console.log('AssetModel: setList: ' + JSON.stringify(value));

        storage.setItem(
            'assetlist', 
            JSON.stringify(value)
        );
    },

    "getList" : function () {
        var storage = window.localStorage;
        
        //Log
        console.log('AssetModel: getList: value-from-localstorage' + storage.getItem('assetlist'));

        return storage.getItem('assetlist');        
    },

    "cachePicklists" : function () {
        var successCallback,
            errorCallback,
            fieldname,
            index,
            storage = window.localStorage;
            
        //Log
        console.log("AssetModel: cachePicklists: Start");            

        for (index in this.picklists){
            
            fieldname = this.picklists[index];
            
            //Log
            console.log("AssetModel: cachePicklists: fetching picklist for " + fieldname);            
            
            //callback for success of fetch picklist request
            successCallback = function(response) {

                //Log
                console.log("AssetModel: cachePicklists: successCallback start");

                if (response.success==true) {
                    
                    //Log
                    console.log("AssetModel: cachePicklists: successCallback saving " + JSON.stringify(response.result));
                    
                    storage.setItem(
                        'picklist_' + fieldname, 
                        JSON.stringify(response.result)
                    );
                }
                
                //Log
                console.log("AssetModel: cachePicklists: successCallback start");                
            };

            errorCallback = function(jqXHR, textStatus, errorThrown) {
                
                //Log
                console.log(
                    'AssetModel: cachePicklists: errorCallback:' + 
                    jqXHR.status + ' ' + 
                    textStatus + ' ' + 
                    jqXHR.responseText + ' ' + 
                    errorThrown
                ); 
                    
            };

            //Sending request to fetch picklist
            RequestBehaviour.send(
                'GET',
                'Assets',
                fieldname,
                successCallback,
                errorCallback
            );

            //Log
            console.log("AssetModel: cachePicklists: End fetching picklist for " + fieldname);

        }
        
        //Log
        console.log("AssetModel: cachePicklists: End");        

    }

});