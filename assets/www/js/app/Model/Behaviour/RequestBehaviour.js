/**
 * @fileoverview Utilities for handling request dispatch / signing.
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var RequestBehaviour = Stapes.create().extend({

   /**
    * Generates Signature based on provided parameters
    * @param {array} param List of values for creating string to sign
    *                       var params = {
    *                           'Verb' : method,
    *                           'Model' : model,
    *                           'Version' : api_version,
    *                           'Timestamp' : timestamp,
    *                           'KeyID' : apikey,
    *                           'UniqueSalt' : uniquesalt
    *                       };
    * 
    * @return {string} This signature of the request to be sent
    * @public
    */

    "generateSignature" : function (params, apisecret) {

        /**
        * The string to be signed
        * @type {string}
        */
        var string_to_sign = '',

        /**
        * The index in params
        * @type {string}
        */
            key,
        /**
        * hmac sha 256
        * @type {string}
        */
            hash,
        /**
        * base 64 of hash
        * @type {string}
        */
            base64;

        //Sort the object
        params = this.sortJSON(params);

        //Log
        console.log('generateSignature: Parameters received: ' + JSON.stringify(params));

        //Generate String to sign
        for (key in params) {
            if (params.hasOwnProperty(key)) {
                string_to_sign += key + params[key];
            }
        }

        //Log
        console.log('generateSignature: String To Sign: ' + string_to_sign);

        hash = CryptoJS.HmacSHA256(string_to_sign, apisecret);

        base64 = hash.toString(CryptoJS.enc.Base64);

        //Log
        console.log('generateSignature: Signature: ' + base64);

        return base64;
    },

   /**
    * Sorts the keys of JS Object 
    * @param {object} o The JS object to be sorted
    * @return {object} The sorted JS object
    * @public
    */

    "sortJSON" : function (o) {
        var sorted = {},
            key,
            a = [];

        for (key in o) {
            if (o.hasOwnProperty(key)) {
                a.push(key);
            }
        }

        a.sort();

        for (key = 0; key < a.length; key = key + 1) {
            sorted[a[key]] = o[a[key]];
        }
        return sorted;
    },

   /**
    * Sends the request
    * @param {object} o The JS object to be sorted
    * @return {object} The sorted JS object
    * @public
    */

    "send" : function (method, model, action, successCallback, errorCallback, data) {
        var signature = '',
            params,
            headers,
            url,
            unixtimestamp = new Date().getTime(),
            delta = window.localStorage.getItem('delta');
        
        if (delta == null) 
        	delta = 0;
        
        if (data == undefined)
        	data = '';
        
        url = AccountModel.scheme + '://' + AccountModel.url + model;            
        
        if (action!='') {
            url = url + '/' + action;
        }

        params = {
            'Verb' : method,
            'Model' : model,
            'Version' : Config.api_version,
            'Timestamp' : new Date(unixtimestamp + (parseInt(delta) * 1000)).toISOString(),
            'KeyID' : AccountModel.apikey,
            'UniqueSalt' : Math.random().toString(36).substr(2, 9)
        };
        
        //Log
        console.log('RequestBehaviour: send: params' + JSON.stringify(params)); 
        
        if (AccountModel.apisecret == '' || AccountModel.url == '' || AccountModel.apisecret == '' ||
        		AccountModel.apisecret == null || AccountModel.url == null || AccountModel.apisecret == null) {
        	
        	errorCallback.call(undefined, {responseText: "{\"error\":{\"code\":\"APPKEY_NO_SET\", \"message\":\"Invalid API Key and signature\"}}"}, '', '');
        	
        	return false;
        }
    
        //Genrate Signature 
        signature = this.generateSignature(params, AccountModel.apisecret);
        
        headers = {
            'X_USERNAME' : UserModel.username,
            'X_PASSWORD' : UserModel.password,
            'X_TIMESTAMP' : params['Timestamp'],
            'X_GIZURCLOUD_API_KEY' : params['KeyID'],
            'X_SIGNATURE' : signature,
            'X_UNIQUE_SALT' : params['UniqueSalt']
        };

        //Log
        console.log('RequestBehaviour: send: header' + JSON.stringify(headers));

        //To make jQuery think the phonegap browser component supports the 
        //W3C "Cross-Origin Resource Sharing" feature 
        //to support cross-domain requests.
        $.support.cors = true;

        //Log
        console.log('RequestBehaviour: send: sending ajax request to' + url);
        
        var errorCallbackWrapper = function(jqXHR, textStatus, errorThrown) {
        	
        	var response = $.parseJSON(jqXHR.responseText);
        	
            //Log
            console.log('RequestBehaviour: errorCallbackWrapper: start ' + jqXHR.responseText);         	
        	
        	if (response) {
	        	if (response.error.code == 'TIME_NOT_IN_SYNC') {
	        		
	        		var delta = window.localStorage.getItem('delta');
	        		delta = parseInt(delta) + parseInt(response.error.time_difference);
	                //Log
	                console.log(
	                    'RequestBehaviour: errorCallbackWrapper: adding delta' + 
	                    delta + '  received ' + parseInt(response.error.time_difference)
	                );         		
	        		
	        		window.localStorage.setItem('delta', delta);
	        		
	        	}
        	}
        	
        	errorCallback.call(undefined, jqXHR, textStatus, errorThrown);
        	
            //Log
            console.log('RequestBehaviour: errorCallbackWrapper: end');          	
        };

        //Send the request
        $.ajax({
            type: params['Verb'],
            beforeSend: function (request)
            {
                var key;
                
                //Log
                console.log(
                    'RequestBehaviour: ajax: beforesend: setting headers' + 
                    JSON.stringify(headers)
                );                
                
                for(key in headers){
                    if (headers.hasOwnProperty(key)) {
                        request.setRequestHeader(key, headers[key]);
                    }
                }
            },
            url: url,
            accept: 'text/json',
            dataType: 'json',
            data: data,
            success: successCallback,
            error: errorCallbackWrapper            
        });
        
        //Log
        console.log('RequestBehaviour: send: sending ajax end');  
    }
});