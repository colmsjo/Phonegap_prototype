/**
 * @fileoverview Model file for HelpDesk
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 28th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var HelpDeskModel = Stapes.create().extend({
    "fields" : '',

    "picklists" : [
        'ticketpriorities', 
        'ticketseverities', 
        'ticketstatus',
        'ticketcategories',
        'tickettype',
        'sealed',
        'reportdamage',
        'damagetype',
        'damageposition',
        'drivercauseddamage',
        'damagereportlocation',
        'straps',
        'plates'
    ],
    
   "picklistCachedCount" : 0,
   
   "picklistNoCachedCount" : 0,
   
   "trouble_ticket_id" : 0,

    "init" : function () {

    },

    "clearPicklist" : function (fieldname) {
        var storage = window.localStorage;
        
        //Log
        console.log("HelpDeskModel: clearPicklist: clear " + fieldname);

        storage.removeItem(
            'picklist_' + fieldname
        );
    },

    "setPicklist" : function (fieldname, value) {
        var storage = window.localStorage;
        
        //Log
        console.log("HelpDeskModel: setPicklist: saving " + fieldname + " " + JSON.stringify(value));

        storage.setItem(
            'picklist_' + fieldname, 
            JSON.stringify(value)
        );
    },

    "getPicklist" : function (fieldname) {
        var storage = window.localStorage;
        
        //Log
        console.log("HelpDeskModel: getPicklist: " + fieldname + ' value-from-localstorage' + storage.getItem('picklist_' + fieldname));

        return storage.getItem('picklist_' + fieldname);        
    },

    "fetchPicklist" : function (fieldname, successCallback, errorCallback) {
            
        //Log
        console.log("HelpDeskModel: getPicklists: fetching picklist for " + fieldname);            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'GET',
            'HelpDesk',
            fieldname,
            successCallback,
            errorCallback
        );
        
        //Log
        console.log("HelpDeskModel: getPicklist: End");        

    },
    
    "fetchList" : function (type, successCallback, errorCallback) {
        
        //Log
        console.log("HelpDeskModel: getLists: fetching list for ");            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'GET',
            'HelpDesk',
            type,
            successCallback,
            errorCallback
        );
        
        //Log
        console.log("HelpDeskModel: getPicklist: End");        

    },    

    "fetchId" : function (id, successCallback, errorCallback) {
        
        //Log
        console.log("HelpDeskModel: getLists: fetching list for ");            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'GET',
            'HelpDesk',
            id,
            successCallback,
            errorCallback
        );
        
        //Log
        console.log("HelpDeskModel: getPicklist: End");        

    },     
    
    "addToQueue" : function (queue_record_value, type) {
    	
        //Log
        console.log("HelpDeskModel: addToQueue: Start");    	
    	
    	if (queue_record_value==null || queue_record_value==undefined)
    		return false;
    	
    	if (type=='SURVEY') {
    		
            //Log
            console.log("HelpDeskModel: addToQueue: SURVEY :" + queue_record_value);     		
    		
    		queue_record_value += '&ticketstatus=Closed';
    		queue_record_value += '&reportdamage=No';
    		queue_record_value += '&ticket_title=Survey Ticket By Driver';
    	} else {
    		
    		queue_record_value.params.ticketstatus = 'Open';
    		queue_record_value.params.reportdamage = 'Yes';
    		queue_record_value.params.ticket_title = 'Damage Report By Driver';
    		
            //Log
            console.log("HelpDeskModel: addToQueue: DAMAGE :" + JSON.stringify(queue_record_value));    		
    	}
    	
        var storage = window.localStorage;
                                           
        var max_queue_id = storage.getItem('max_queue_id');
        var queue = $.parseJSON(storage.getItem('queue'));
        
        var queue_record = {};
        
        if (max_queue_id ==  null)
        	max_queue_id = 0;
        
        if (queue ==  null)
        	queue = [];        
        
        queue_record.id = parseInt(max_queue_id) + 1;
        queue_record.value = queue_record_value;
        queue_record.status = 'PENDING';
        queue_record.type = type;
        queue_record.created = new Date().getTime();
        
        //Log
        console.log("HelpDeskModel: addToQueue: DAMAGE :" + JSON.stringify(queue_record));        
        
        queue.push(queue_record);
        
        storage.setItem('queue', JSON.stringify(queue)); 
        storage.setItem('max_queue_id', parseInt(max_queue_id) + 1);
        
        //Log
        console.log("HelpDeskModel: addToQueue: End");        
        
    },
    

    "deleteFromQueue" : function (queue_records) {
    	
        //Log
        console.log("HelpDeskModel: deleteFromQueue: Start" + JSON.stringify(queue_records));    	
    	
    	if (queue_records == null || queue_records == undefined)
    		return false;
    	
        var storage = window.localStorage;                                          
        var queue = $.parseJSON(storage.getItem('queue'));
        
        if (queue ==  null)
        	queue = []; 
                
        //Log
        console.log("HelpDeskModel: deleteFromQueue: before delete " + JSON.stringify(queue));         
        
        for (var index=0;index<queue.length;index++) {
        	
	        if (queue[index] != null) {
	        	
	            //Log
	            console.log("HelpDeskModel: deleteFromQueue: checking index " + queue[index].id);   
	            
	            //Log
	            console.log("HelpDeskModel: deleteFromQueue: checking boolean " + queue_records.indexOf(queue[index].id.toString()));            
	        	
	        	if (queue_records.indexOf(queue[index].id.toString())!=-1) {
	        		
	                //Log
	                console.log("HelpDeskModel: deleteFromQueue: deleting record with id " + queue[index].id);         		
	        		
	        		queue.splice(index, 1);
	        		index = -1;
	        	} 
        	} else {
        		queue.splice(index, 1);
        	}
        }
        
        //Log
        console.log("HelpDeskModel: deleteFromQueue: after delete " + JSON.stringify(queue));         
        
        storage.setItem('queue', JSON.stringify(queue)); 
        
        //Log
        console.log("HelpDeskModel: deleteFromQueue: End");        
        
    },    
    
    "reportSurvey" : function(data, successCallback, errorCallback) {
        //Log
        console.log("HelpDeskModel: reportSurvey: Reporting Survey " + data);            

        //Sending request to fetch picklist
        RequestBehaviour.send(
            'POST',
            'HelpDesk',
            '',
            successCallback,
            errorCallback,
            data
        );
        
        //Log
        console.log("HelpDeskModel: reportSurvey: End");     	
    },
    
    "reportDamage" : function(data, successCallback, errorCallback) {
        //Log
        console.log("HelpDeskModel: reportDamage: Reporting Survey " + JSON.stringify(data));  
        

        var fileURI = data.files[0];
        
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.fileName=fileURI.substr(fileURI.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";
        var method = 'POST';
        var model = 'HelpDesk'
        var unixtimestamp = new Date().getTime();
        var delta = window.localStorage.getItem('delta');
        
        if (delta == null) 
        	delta = 0;  
        
        var headers = {
                'Verb' : method,
                'Model' : model,
                'Version' : Config.api_version,
                'Timestamp' : new Date(unixtimestamp + (parseInt(delta) * 1000)).toISOString(),
                'KeyID' : AccountModel.apikey,
                'UniqueSalt' : Math.random().toString(36).substr(2, 9)
        };
        
        var signature = RequestBehaviour.generateSignature(headers, AccountModel.apisecret);
        
        options.params = data.params;        
        
        options.params.headers = {
            'X_USERNAME' : UserModel.username,
            'X_PASSWORD' : UserModel.password,
            'X_TIMESTAMP' : headers['Timestamp'],
            'X_GIZURCLOUD_API_KEY' : headers['KeyID'],
            'X_SIGNATURE' : signature,
            'X_UNIQUE_SALT' : headers['UniqueSalt']
        };     
        
        //Log
        console.log("HelpDeskModel: reportDamage: Headers " + JSON.stringify(options.params.headers));        
        
        //Log
        console.log("HelpDeskModel: reportDamage: Starting File Transfer: " + AccountModel.url + "Helpdesk " + fileURI);        

        var ft = new FileTransfer();
        ft.upload(fileURI, AccountModel.scheme + "://" + AccountModel.url + model, successCallback, errorCallback, options);        
        
        //Log
        console.log("HelpDeskModel: reportDamage: End");     	
    }
});