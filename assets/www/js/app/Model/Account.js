var AccountModel = Stapes.create().extend({   
    
    "apikey" : '',

    "apisecret" : '',
    
    "url" : '', 
    
    "imagequality" : '', 
    
    "scheme" : '',     
    
    "init" : function () {
        
        //Log
        console.log('Account Model: Init: Start');        
        
        var storage = window.localStorage;
        
        this.apikey = storage.getItem('apikey');
        
        this.apisecret = storage.getItem('apisecret');
        
        this.url = storage.getItem('url'); 
        
        this.scheme = storage.getItem('scheme');
        
        this.imagequality = storage.getItem('imagequality');         
        
        //Log
        console.log(
            'Retrieving account info: API_KEY' + 
            this.apikey + 
            ' API_SECRET' + this.apisecret +
            ' URL' + this.url + ' IMAGEQUALITY' + this.imagequality + ' SCHEME ' + this.scheme
        );        
        
        //Log
        console.log('Account Model: Init: End');        

    },
    
    "save" : function () {
        //Log
        console.log('Account Model: Save: Start');
        
        var storage = window.localStorage;
        
        storage.setItem('apikey', this.apikey);
        
        storage.setItem('apisecret', this.apisecret);

        storage.setItem('url', this.url);
        
        storage.setItem('scheme', this.scheme);

        storage.setItem('imagequality', this.imagequality);        

        //Log
        console.log(
            'Saving Account info: API_KEY' + this.apikey + 
            ' API_SECRET' + this.apisecret +
            ' URL' + this.url +
            ' SCHEME ' + this.scheme +
            ' IMAGE QUALITY ' + this.imagequality 
        );
        
        //Log
        console.log('Account Model: Save: End');        
    }
});