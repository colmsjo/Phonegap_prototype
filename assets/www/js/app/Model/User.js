/**
 * @fileoverview Model file for Users
 * @author anshuk-kumar@essindia.co.in (Anshuk Kumar)
 * 
 * 27th December 2012
 * 
 * @preserve Copyright Gizur AB 2012
 * 
 */

/**
 * JSLint needs this
 */

"use strict";

var UserModel = Stapes.create().extend({
    "username" : '',

    "password" : '',

    "init" : function () {

        var storage = window.localStorage;

        this.username = storage.getItem('username');
        this.password = storage.getItem('password');
    },

    "isSessionActive" : function () {

        var storage = window.localStorage;
        
        //Log 
        console.log('UserModel: isSessionActive: ' + storage.getItem('username') + ' ' + storage.getItem('password'));

        if (storage.getItem('username') == null ||
            storage.getItem('password') == null) {
        	
            //Log 
            console.log('UserModel: isSessionActive: returing false' );        	
        	
        	return false;
        }
        
        //Log 
        console.log('UserModel: isSessionActive: returing true');         
        
        return true;
    },    
    
    "saveCredentials" : function () {

        var storage = window.localStorage;
        
        //Log
        console.log(
            'UserModel: saveCredentials: ' + 
            ' Username ' + UserModel.username +
            ' Password ' + UserModel.password
        );        

        //storing to localstorage
        storage.setItem('username', UserModel.username);
        storage.setItem('password', UserModel.password);
        
        //Log
        console.log('UserModel: saveCredentials: End');        
    },

    "clearCredentials" : function () {
    	
        //Log
        console.log('UserModel: clearCredentials: Start');    	

        var storage = window.localStorage;

        //removing from localstorage
        storage.removeItem('username');
        storage.removeItem('password');
        
        //Log
        console.log('UserModel: clearCredentials: End');        
        
    },

    "login" : function (un, pw, errorCallback, successCallback) {

        this.username = un;
        this.password = pw;

        RequestBehaviour.send(
            'POST',
            'Authenticate',
            'login',
            successCallback,
            errorCallback
        );
    }

});

UserModel.on('cachereceived', function(args){
    
    var type = args[0];
    var mode = args[1];
    
    console.log('cache received Start: ' + type + '  ' + mode);
    
    if (type=='HelpDesk' && mode=='success')
        HelpDeskModel.picklistCachedCount =  HelpDeskModel.picklistCachedCount + 1;
    
    if (type=='HelpDesk' && mode=='error')
        AssetModel.picklistNoCachedCount =  AssetModel.picklistNoCachedCount + 1;    
    
    if (type=='Asset' && mode=='success')
        AssetModel.picklistCachedCount =  AssetModel.picklistCachedCount + 1;

    if (type=='Asset' && mode=='error')
        AssetModel.picklistNoCachedCount =  AssetModel.picklistNoCachedCount + 1;
    
    if (type=='AssetList' && mode=='success')
        AssetModel.listCached =  1;

    if (type=='AssetList' && mode=='error')
        AssetModel.listCached =  0;    

    console.log('cache recevied Count:  ' + AssetModel.picklistCachedCount + ' ' + HelpDeskModel.picklistCachedCount);
    console.log('cache recevied Length:  ' + AssetModel.picklists.length + ' ' + HelpDeskModel.picklists.length);
    
    var total = AssetModel.picklists.length + HelpDeskModel.picklists.length + 1;
    var completed = AssetModel.picklistCachedCount + HelpDeskModel.picklistCachedCount + AssetModel.listCached;
    var status = (completed / total) * 100;
    
    console.log('cache progress status:  Total:' + total + '  Completed:' + completed + '  Status:' + status);
    
    //Show Popup
    $('#message h3:last').html(' Building Cache: '+ parseInt(status) + '% Completed');
    
    
    if (HelpDeskModel.picklistCachedCount == HelpDeskModel.picklists.length &&
        AssetModel.picklistCachedCount == AssetModel.picklists.length &&
        AssetModel.listCached == 1
       ) {

        console.log('cache received End');
        
        $('#button-save').button('enable').button('refresh');
        
        //redirect to different page
        $.mobile.changePage("survey.html", { transition: "none"});
        return false;

    }   
    
    if ((HelpDeskModel.picklistCachedCount + HelpDeskModel.picklistNoCachedCount) == HelpDeskModel.picklists.length &&
        (AssetModel.picklistCachedCount + AssetModel.picklistNoCachedCount) == AssetModel.picklists.length &&
        AssetModel.listCached != null) {
    	console.log('UserModel: Cache Received: Error Couldnt get PickList');
    	
    	console.log('UserModel: Cache Received: HelpDeskModel.picklistNoCachedCount ' + HelpDeskModel.picklistNoCachedCount);
    	console.log('UserModel: Cache Received: HelpDeskModel.picklistCachedCount ' + HelpDeskModel.picklistCachedCount);
    	console.log('UserModel: Cache Received: HelpDeskModel.picklists.length ' + HelpDeskModel.picklists.length);
    	
    	console.log('UserModel: Cache Received: AssetModel.picklistNoCachedCount ' + AssetModel.picklistNoCachedCount);
    	console.log('UserModel: Cache Received: AssetModel.picklistCachedCount ' + AssetModel.picklistCachedCount);
    	console.log('UserModel: Cache Received: AssetModel.picklists.length ' + AssetModel.picklists.length);
    	
        $('#button-save').button('enable').button('refresh');
    	
    	$('#message').html('<h3 style="color:orange;">Unable to build cache .. try again</h3>');                            
    } 
    
    console.log('cache received End Checking');
});