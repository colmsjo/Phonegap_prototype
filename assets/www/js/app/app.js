document.addEventListener("deviceready", function() {

window.plugins.webintent.getUri(function(url) {
    if(url !== "" && url !== null ) {
        console.log("URL received:" + url);    	
        // url is the url that was passed to onNewIntent
        if (url.indexOf('?')!==-1) {
            var qs = url.split('?')[1].split('&');
            for(var index in qs) {
            	console.log("Query string parts: " + qs[index]);
                var kvpair = qs[index].split('=');
                if (kvpair[0]=='GIZURCLOUD_API_KEY') {
                    AccountModel.apikey = kvpair[1];
                }
                if (kvpair[0]=='GIZURCLOUD_SECRET_KEY') {
                    AccountModel.apisecret = kvpair[1];
                }
                if (kvpair[0]=='GIZURCLOUD_API_URL') {
                    AccountModel.url = kvpair[1] + '/';
                }
                if (kvpair[0]=='GIZURCLOUD_HTTP_SCHEME') {
                    AccountModel.scheme = kvpair[1];
                }                
            }
            AccountModel.save();
        }
    }
});

$.ajax({async:false});
$.mobile.loadPage("survey.html");
$.mobile.loadPage("queue.html");
$.mobile.loadPage("setup.html");
$.mobile.loadPage("about.html");
$.mobile.loadPage("damages.html");
$.mobile.loadPage("damageadd.html");
$.ajax({async:true})

IndexController.init();

}, false);
$(document).unbind('pagechange');
$(document).on('pagechange', function(event, data){
	var controller;
	
	//Log
	//console.log("Load Complete for : " + data.url);	
	
	if (data.toPage instanceof Object)
		controller = data.toPage.attr('id').replace("page-", "");
	else
		controller = data.toPage.replace(".html", "");
	
	//Log
	console.log("document pagechange controllername : " + controller);
	
	controller = controller.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) + 'Controller';
	
	var fn = window[controller];
	
	if (typeof fn === 'object') {
		
		//Log
		console.log("document pagechange calling controller fucntion name : " + controller);		
		
		fn.init();
	} else {
		
		//Log
		console.log("document pagechange Not a Function : " + controller);		
		
	}
	
});